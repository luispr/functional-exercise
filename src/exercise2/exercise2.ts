export function myMap<T, U>(
  values: T[], // Vector de valores de tipo T
  transform: (value: T) => U // Función que transforma un valor de tipo T en un valor de tipo U
): U[] {

  let results: U[] = [];

  for (const value of values) {
    results.push(transform(value));
  }

  return results;
}

export function myMap2<T, U>(
  values: T[], // Vector de valores de tipo T
  transform: (value: T) => U // Función que transforma un valor de tipo T en un valor de tipo U
): U[] {

  let results: U[] = [];

  for (let i = 0; i < values.length; i++) {
    results.push(transform(values[i]));
  }

  return results;
}

export function myMap3<T, U>(
  values: T[], // Vector de valores de tipo T
  transform: (value: T) => U // Función que transforma un valor de tipo T en un valor de tipo U
): U[] {
  return values.map(transform);
}

// Map es una version mas especifica de reduce
// export function myMap3<T, U>(
//   values: T[], // Vector de valores de tipo T
//   transform: (value: T) => U // Función que transforma un valor de tipo T en un valor de tipo U
// ): U[] {
//   return values.reduce((results, value) => [...results, transform(value)]);

// }