export type Tree = number | Tree[];

// const reduceTree = (tree: Tree, fn: (acc: number, val: number) => number, acc: number): number => {
//   if (typeof tree === 'number'){
//     return tree;
//   }else{
//     const numbers = tree.map(sumTree);

//     if (numbers.length == 0){
//       throw new Error('Empty tree');
//     }else{
//       return numbers.reduce(combine);
//     }
//   }

// }


export const sumTree = (tree: Tree): number => {
  if (typeof tree === 'number'){
    return tree;
  }
  else{
    return tree.map(sumTree).reduce((acc, val) => acc + val, 0);
  }
  
  //throw new Error('Not implemented');
};

export const maxTree = (tree: Tree): number => {
  if (typeof tree === 'number'){
    return tree;
  }else{
    const numbers = tree.map(maxTree);
    if (numbers.length == 0){
      throw new Error('Empty tree');
    }else{
      return numbers.reduce((acc, val) => Math.max(acc, val), numbers[0]);
    }
  }
};
