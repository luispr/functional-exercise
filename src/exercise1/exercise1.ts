function fib(n: number): number {
  if (n < 2) return n;
  return fib(n - 1) + fib(n - 2);
}


export const fibonacci = (n: number): number => {
  return fib(n);
};
