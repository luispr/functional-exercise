import { Tree, FunctionEnvironment, ValueEnvironment } from "./types";

export const evalTree = (
  tree: Tree,
  functionEnvironment: FunctionEnvironment,
  valueEnvironment: ValueEnvironment
): number => {
  if (typeof tree === 'number'){
    return tree;
  }
  else if (typeof tree === 'string'){
    return valueEnvironment[tree];
  }
  else{
    const fn = functionEnvironment[tree.call];
    const args = tree.args.map(arg => evalTree(arg, functionEnvironment, valueEnvironment));
    return fn(args);
  }
}