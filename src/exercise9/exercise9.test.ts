import { evalTree } from "./exercise9";
import { FunctionEnvironment, ValueEnvironment, Tree } from "./types";

const functions: FunctionEnvironment = {
  '+': args => args.reduce((x, y) => x + y, 0),

  '-': args => args.length === 0
    ? 0
    : args.length === 1
    ? args[0]
    : args.reduce((x, y) => x - y),

  '*': args => args.reduce((x, y) => x * y, 1),

  '/': args => args.length === 0
    ? 1
    : args.length === 1
    ? args[0]
    : args.reduce((x, y) => x / y),
};

const values: ValueEnvironment = {
  'one': 1,
  'two': 2
};

describe('evalTree', () => {
  it('returns a leaf value', () => {
    expect(evalTree(3, functions, values)).toBe(3);
  });

  it('returns a leaf variable', () => {
    expect(evalTree('one', functions, values)).toBe(1);
  });

  it('returns a function call', () => {
    const program: Tree = {
      call: '+',
      args: [1, 'two']
    };
    expect(evalTree(program, functions, values)).toBe(3);
  });

  it('returns a nested call', () => {
    const program: Tree = {
      call: '-',
      args: [
        10,
        {
          call: '*',
          args: [4, 'two']
        }
      ]
    };
    expect(evalTree(program, functions, values)).toBe(2);
  });
});