function sum(numbers: number[]): number {
  let initial_value = 0;
  return numbers.reduce( (a,b) => a+b, initial_value)
}

export const average = (numbers: number[]): number => {
  return  sum(numbers) / numbers.length;
};

export const average2 = (numbers: number[]): number => {
  return numbers.reduce( (a,b) => a+b, 0) / numbers.length;
}

export const average3 = (numbers: number[]): number => {
  let sum = 0;
  for (const number of numbers) {
    sum += number;
  }
  return sum / numbers.length;
}

export const average4 = (numbers: number[]): number => {
  let sum = 0;
  for (let i = 0; i < numbers.length; i++) {
    sum += numbers[i];
  }
  return sum / numbers.length;
}