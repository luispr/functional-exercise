import { Category } from './types';

const getPriceAux = (category: Category) => {
  switch (category) {
    case Category.One:
      return 100;
    case Category.Two:
      return 200;
    case Category.Five:
      return 500;
  }
};

export const getPrice = (category: Category, offer: boolean) => {
  const discount = offer ? 25 : 0; 
  return getPriceAux(category) - discount;
};
