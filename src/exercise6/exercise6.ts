export type Action = {
  canRunAfterDelay: number;
  mustRunBeforeDelay: number;
  command: string;
};

/** Determine all actions that can run at the time the first action must run, and when that is */
export const determineWhenToRun = (
  actions: Action[],
): null | { delay: number; actions: Action[] } => {

  
  if (actions.length == 0){
    return null;
  }
  
  const firstAction = actions.map
    (actions => actions.mustRunBeforeDelay)
    .reduce((a, b) => Math.min(a, b));

  return {
    delay: firstAction,
    actions: actions.filter(action => action.canRunAfterDelay <= firstAction)
  }
  //throw new Error('Not implemented');
};
