import { myReduce } from "./exercise3";

describe('myReduce', () => {
  it('Empty list', () => {
    expect(myReduce(1, [], () => null)).toStrictEqual(1);
  });

  it('One element list', () => {
    expect(myReduce(1, [2], (acc, val) => acc - val)).toStrictEqual(-1);
  });

  it('Two element list', () => {
    expect(myReduce(1, [2, 3], (acc, val) => acc * val)).toStrictEqual(6);
  });
});