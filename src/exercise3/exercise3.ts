export function myReduce<T, U>(
  initialValue: U,
  values: T[],
  combine: (accumulator: U, nextValue: T) => U,
): U {

  let total = initialValue;

  for (const value of values) {
    total = combine(total, value);
  }

  return total;
}

export function myReduce2<T, U>(
  initialValue: U,
  values: T[],
  combine: (accumulator: U, nextValue: T) => U,
): U {
  return values.reduce(combine, initialValue);
}

export function myReduce3<T, U>(
  initialValue: U,
  values: T[],
  combine: (accumulator: U, nextValue: T) => U,
): U {

    if (values.length === 0) {
      return initialValue;
    }

    return myReduce3(combine(initialValue, values[0]), values.slice(1), combine);

}